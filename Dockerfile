FROM openjdk:8-jdk

ENV USER=builder
ENV HOME_PATH=/home/${USER}
ENV ANDROID_SDK_TOOLS=4333796
ENV ANDROID_BUILD_TOOLS=32.0.0
ENV ANDROID_COMPILE_SDK=31
ENV FLUTTER_SDK=2.10.2
ENV ANDROID_HOME=/opt/android-linux
ENV FLUTTER_HOME=/opt/flutter

WORKDIR /opt

ENV PATH="${PATH}:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}}/platform-tools:${FLUTTER_HOME}/bin"

# Create Config Folder and File
RUN mkdir -p ~/.android && touch ~/.android/repositories.cfg \
        && mkdir -p ${HOME_PATH}/app \
        && apt-get update \
        && apt-get install -y \
                clang \
                cmake \
                ninja-build \
                pkg-config \
                libgtk-3-dev \
        && rm -rf /var/lib/apt/lists/*


# Install SDK Tools
RUN wget -c https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip -qO sdk.zip \
        && unzip sdk.zip -d android-linux \
        && rm -rf sdk.zip \
        && echo y | sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" > /dev/null \
        && echo y | sdkmanager "platform-tools" > /dev/null \
        && echo y | sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" > /dev/null \
        && echo y | sdkmanager "cmdline-tools;latest" > /dev/null \
        && echo y | sdkmanager --licenses > /dev/null \
        && sdkmanager --update

RUN wget -c https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${FLUTTER_SDK}-stable.tar.xz -qO flutter_sdk.tar.xz \
        && tar -xvf flutter_sdk.tar.xz \
        && rm -rf flutter_sdk.tar.xz

# Change Work Dir
WORKDIR ${HOME_PATH}/app

# Disable analytic
RUN flutter config --no-analytics

# Set Flutter to Precache
RUN flutter precache

# Run Flutter Doctor to Accept The License
RUN yes | flutter doctor --android-licenses

# Run Flutter Doctor
RUN flutter doctor -v

